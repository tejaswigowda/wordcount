var words = {};
var queue = [document.body];
var curr;
var byProperty = function(prop) {
    return function(a,b) {
        if (typeof a[prop] == "number") {
            return (b[prop] - a[prop]);
        } else {
            return ((a[prop] < b[prop]) ? -1 : ((a[prop] > b[prop]) ? 1 : 0));
        }
    };
};

while (curr = queue.pop()) {
    for (var i = 0; i < curr.childNodes.length; ++i) {
        switch (curr.childNodes[i].nodeType) {
            case Node.TEXT_NODE : // 3
            	var temp = curr.childNodes[i].textContent.split(/\W+/); 
                for (var j = 0; j < temp.length; j++) {
					var w = temp[j].toLowerCase();
                    if(w.charCodeAt(0) < 65) continue; //ignore words starting with numbers
                    if(w.length < 4) continue; // ignore small words
                    if(w.indexOf("_") >= 0) continue; // ignore possible programming vars
					var temp1 = Object.keys(words);
					if(temp1.indexOf(w) >= 0){
						words[w] += 1;
					}
					else{
						words[w] = 1;
					}
                }
                break;
            case Node.ELEMENT_NODE : // 1
                queue.push(curr.childNodes[i]);
                break;
        }
    }
}

var wordsDict = [];
var ws = Object.keys(words);
for (var i=0; i< ws.length; i++){
	wordsDict[wordsDict.length] = {"word": ws[i], "times": words[ws[i]]};
}

wordsDict.sort(byProperty("times"));
var max = wordsDict[0].times;
var min = wordsDict[wordsDict.length-1].times;


wordsDict = wordsDict.slice(0,100);


wordsDict.sort(byProperty("word"));

alert("Word Count Complete! Please enable popups for this bookmarklet to work correctly.");
var outS = "";
outS += "<div style='display:block; width:460px;height:auto;'>";

for (var i = 0; i < wordsDict.length; i++)
{
	var sz = (wordsDict[i].times - min)*100/(max-min) + 100;
	outS += "<span style='margin-left: 10px;font-size:" + sz + "%'> " + wordsDict[i].word+ "</span>"
}

outS += "</div>";

var win = window.open("", "Word Count Visualization", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, width=480, height=300");




win.document.write(outS);
